const express = require('express');
const app = express();

const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({extended: false}));

//Routes import
const pokemonRoutes = require('./routes/pokemon.js');

app.use('/api/v1/pokemon', pokemonRoutes);

app.listen(port, ()=>{
    console.log(`Server running on port ${port} ...`);
});