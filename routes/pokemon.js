const express = require('express');
const router = express.Router();
const pokemonModel = require('../models/pokemon-model.js');

router.get('/', (req, res)=>{
    const apiRes = pokemonModel.getPokemon(req, res);
    res.json(apiRes).end();
});

router.post('/', (req, res)=>{

});

router.put('/', (req, res)=>{

});

router.delete('/', (req, res)=>{

});

module.exports = router;